# OpenWeather



## Tech
* [Coroutines](https://developer.android.com/kotlin/coroutines) - To execute asynchronous tasks.
* [Hilt](https://developer.android.com/training/dependency-injection/hilt-android) - Automatized dependency injection library.
* [Hilt](https://dagger.dev/hilt/) - Hilt documentation on dagger.dev.
* [Retrofit](https://square.github.io/retrofit/) - To make HTTP requests.
* [ViewModel](https://developer.android.com/jetpack/androidx/releases/lifecycle) - Link for ViewModel dependencies.
* [Glide](https://github.com/bumptech/glide) - To load images in ImageViews.

- Here we have used Hilt as Dependency Injection
- Written Unit test cases using Mockito and JUnit.
- This application designed with MVVM Architecture.

**Model:** The model represents the data and business logic of the application. It encapsulates the data and provides methods to manipulate and access it. 
            In MVVM, the model is independent of the user interface (UI) and does not have any knowledge of how it is being presented to the user.

**View:** The view represents the user interface and is responsible for displaying the data to the user and capturing user interactions.
             It is typically implemented using markup languages like XAML or HTML and is designed to be as lightweight as possible, with minimal logic.

**ViewModel:** The ViewModel acts as an intermediary between the view and the model. It contains the presentation logic and exposes the data and commands that the view needs to display and interact with. The ViewModel is responsible for preparing and formatting the data from the model in a way that is suitable for the view.

Sample screen shots
![](Screenshot_20230613_201029.png)

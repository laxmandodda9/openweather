package com.demo.openweatherapp.data.repoimpl

import com.demo.openweatherapp.data.model.WeatherResponse
import com.demo.openweatherapp.data.service.WeatherService
import com.demo.openweatherapp.domain.model.Main
import com.demo.openweatherapp.domain.model.Weather
import com.demo.openweatherapp.domain.model.WeatherModel
import com.demo.openweatherapp.domain.model.Wind
import com.demo.openweatherapp.domain.repo.WeatherRepository
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(private val weatherService: WeatherService) :
    WeatherRepository {
    override suspend fun getWeatherData(cityName: String): WeatherModel? {
        val response = weatherService.getWeatherData(cityName)
        return if (response.isSuccessful) {
            val weatherResponse = response.body()
            // Convert the WeatherResponse to WeatherModel object
            weatherResponse?.toWeatherModel() ?: throw Exception("Invalid response")
        } else {
            null
        }
    }

    private fun WeatherResponse.toWeatherModel(): WeatherModel {
        val localWeather = weather?.firstOrNull()
        // Perform the necessary conversion from WeatherResponse to WeatherModel
        return WeatherModel(
            weather = Weather(
                main = localWeather?.main.orEmpty(),
                description = localWeather?.description.orEmpty(),
                icon = localWeather?.icon.orEmpty()
            ),
            main = Main(
                temp = main?.temp ?: 0.0,
                feels_like = main?.feels_like ?: 0.0,
                temp_min = main?.temp_min ?: 0.0,
                temp_max = main?.temp_max ?: 0.0,
                pressure = main?.pressure ?: 0,
                humidity = main?.humidity ?: 0
            ),
            wind = Wind(
                wind?.speed ?: 0.0,
                wind?.deg ?: 0,
                wind?.gust ?: 0.0
            )
        )
    }
}

package com.demo.openweatherapp.presentation.fragment

import android.app.AlertDialog
import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.demo.openweatherapp.R

abstract class BaseFragment(id: Int) : Fragment(id) {
    protected var progressDialog: ProgressDialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressDialog = ProgressDialog(requireContext())
    }

    /**
     * Method to show progressDialog
     */
    internal fun showProgressDialog() {
        progressDialog?.apply {
            setMessage(getString(R.string.msg_loading))
            show()

        }
    }

    /**
     * Method to display Error Dialog
     */
    internal fun showErrorDialog() {
        val builder = AlertDialog.Builder(requireContext())
        builder.apply {
            setTitle(getString(R.string.error_dialog_title))
            setMessage(getString(R.string.error_dialog_message))
            setPositiveButton(android.R.string.ok) { dialog, which ->
                dialog.dismiss()
            }
            show()
        }
    }
}
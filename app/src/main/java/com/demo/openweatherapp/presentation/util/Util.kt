package com.demo.openweatherapp.presentation.util

import android.content.Context
import com.demo.openweatherapp.presentation.util.Constants.EMPTY_STRING

private const val PREF_NAME = "open_weather"
const val KEY_CITY = "key_city"

/**
 * Method to store search city Info
 */
fun storeCityInfo(context: Context, city: String) {
    if (city.isEmpty()) return
    val sharedPreference = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    sharedPreference.edit().apply {
        putString(KEY_CITY, city)
        apply()
    }
}

/**
 * Method to fetch previous search city info
 */
fun getPreviousSearchedCityFromPref(context: Context): String {
    val sharedPreference = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    return sharedPreference.getString(KEY_CITY, EMPTY_STRING).orEmpty()
}
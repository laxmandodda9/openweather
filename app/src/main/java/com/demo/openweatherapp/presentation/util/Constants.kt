package com.demo.openweatherapp.presentation.util

/**
 *
 */
object Constants {
    const val BASE_URL = "https://api.openweathermap.org/"
    const val IMAGE_URL = "https://openweathermap.org/img/wn/%s.png"
    const val EMPTY_STRING = ""
}
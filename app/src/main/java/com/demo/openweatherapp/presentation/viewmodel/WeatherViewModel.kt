package com.demo.openweatherapp.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo.openweatherapp.domain.model.WeatherModel
import com.demo.openweatherapp.domain.usecase.WeatherUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 *
 */
@HiltViewModel
class WeatherViewModel @Inject constructor(
    private val weatherUseCase: WeatherUseCase
) :
    ViewModel() {
    private val _weatherData = MutableLiveData<WeatherModel?>()
    val weatherData: MutableLiveData<WeatherModel?> = _weatherData

    /**
     * Method to fetch Weather Info
     * @param cityName : City
     */
    fun fetchWeatherData(cityName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = weatherUseCase.getWeatherData(cityName)
            _weatherData.postValue(result)
        }
    }
}

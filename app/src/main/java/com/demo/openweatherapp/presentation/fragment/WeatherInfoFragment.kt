package com.demo.openweatherapp.presentation.fragment

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.*
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.demo.openweatherapp.R
import com.demo.openweatherapp.databinding.FragmentWeatherInfoBinding
import com.demo.openweatherapp.domain.model.WeatherModel
import com.demo.openweatherapp.presentation.util.Constants.EMPTY_STRING
import com.demo.openweatherapp.presentation.util.Constants.IMAGE_URL
import com.demo.openweatherapp.presentation.util.getPreviousSearchedCityFromPref
import com.demo.openweatherapp.presentation.util.storeCityInfo
import com.demo.openweatherapp.presentation.util.viewBinding
import com.demo.openweatherapp.presentation.viewmodel.WeatherViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class WeatherInfoFragment : BaseFragment(R.layout.fragment_weather_info), LocationListener {

    private var currentLocationCity: String = EMPTY_STRING
    private lateinit var locationManager: LocationManager

    private val weatherViewModel: WeatherViewModel by viewModels()
    private val binding by viewBinding(FragmentWeatherInfoBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchLastUpdatedCityInfo()

        binding.inputCityName.editText?.addTextChangedListener {
            binding.pressMe.isEnabled = it.toString().isNotEmpty()
        }

        weatherViewModel.weatherData.observe(viewLifecycleOwner) {
            progressDialog?.dismiss()
            it?.let {
                updateWeatherInfo(it)
            } ?: showErrorDialog()
        }

        binding.pressMe.setOnClickListener {
            val cityName = binding.inputCityName.editText?.text.toString()
            fetchWeatherInfo(cityName)
        }
    }

    /**
     * Method to fetch Weather Info based on City
     * @param cityName : City
     */
    private fun fetchWeatherInfo(cityName: String) {
        showProgressDialog()
        weatherViewModel.fetchWeatherData(cityName)
    }

    /**
     * Method to fetch last updated city info
     */
    private fun fetchLastUpdatedCityInfo() {
        val lastUpdatedCityName = getPreviousSearchedCityFromPref(binding.root.context)
        if (lastUpdatedCityName.isNotEmpty()) {
            binding.inputCityName.editText?.setText(lastUpdatedCityName)
            fetchWeatherInfo(lastUpdatedCityName)
            if (this::locationManager.isInitialized) {
                locationManager.removeUpdates(this)
            }
        } else {
            fetchCurrentLocation()
        }
    }

    /**
     * Method to update Weather Info
     * @param weatherModel : Weather Model
     */
    private fun updateWeatherInfo(weatherModel: WeatherModel) {
        with(weatherModel) {
            val cityName = binding.inputCityName.editText?.text.toString()
            if (cityName.isEmpty()) {
                binding.tvCityInfo.text = getString(R.string.msg_present_city, currentLocationCity)
            } else {
                binding.tvCityInfo.text = getString(R.string.str_city, cityName)
            }
            storeCityInfo(binding.root.context, cityName)
            binding.tvTemp.text = main?.temp.toString() + " \u2109"
            binding.weatherDescription.text = getString(R.string.weather, weather.description)
            binding.feelsLike.text = getString(R.string.feels_like, main.feels_like.toString())
            binding.tvHumidity.text = getString(R.string.humidity, main.humidity.toString())
            val url = String.format(IMAGE_URL, weather.icon)
            Glide.with(this@WeatherInfoFragment).load(url).into(binding.weatherIcon)
            binding.tempValues.text = String.format(
                getString(
                    R.string.temp_values,
                    main.temp_min.toString(),
                    main.temp_min.toString()
                )
            )
        }
    }

    /**
     * Method to fetch current Location
     */
    private fun fetchCurrentLocation() {

        context?.let { context ->
            if (ActivityCompat.checkSelfPermission(
                    context, Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context, Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                locationManager =
                    activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 0, 0F, this
                )
                return
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        val geocoder = Geocoder(requireContext(), Locale.getDefault())
        val addresses: List<Address>? =
            geocoder.getFromLocation(location.latitude, location.longitude, 1)
        addresses?.let {
            currentLocationCity = addresses[0].locality
            locationManager.removeUpdates(this)
            weatherViewModel.fetchWeatherData(currentLocationCity)
        }
    }
}
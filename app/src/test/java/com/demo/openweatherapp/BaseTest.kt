package com.demo.openweatherapp

import org.junit.Before
import org.mockito.Mockito
import org.mockito.MockitoAnnotations.openMocks

open class BaseTest {

    @Before
    open fun setup() {
        openMocks(this)
    }

    fun <T> any(): T = Mockito.any<T>()
}
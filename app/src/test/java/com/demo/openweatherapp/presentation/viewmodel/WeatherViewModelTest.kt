package com.demo.openweatherapp.presentation.viewmodel

import com.demo.openweatherapp.BaseViewModelTest
import com.demo.openweatherapp.domain.model.WeatherModel
import com.demo.openweatherapp.domain.usecase.WeatherUseCase
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertNull
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class WeatherViewModelTest : BaseViewModelTest() {

    @Mock
    private lateinit var weatherUseCase: WeatherUseCase

    private lateinit var viewModel: WeatherViewModel

    @Before
    override fun setup() {
        super.setup()
        viewModel = WeatherViewModel(weatherUseCase)
    }

    @Test
    fun testFetchWeatherData_success() = runTest {
            val mockWeatherModel = Mockito.mock(WeatherModel::class.java)
            Mockito.`when`(weatherUseCase.getWeatherData(TEST_CITY)).thenReturn(
                mockWeatherModel
            )
            getLiveData(viewModel.weatherData) {
                viewModel.fetchWeatherData(
                    TEST_CITY
                )
            }
            assertNotNull(viewModel.weatherData.value)
        }

    @Test
    fun testFetchWeatherData_failure() = runTest {
            Mockito.`when`(weatherUseCase.getWeatherData(Mockito.anyString())).thenReturn(
                null
            )
            viewModel.fetchWeatherData(
                TEST_CITY
            )
            getLiveData(viewModel.weatherData) {
                viewModel.fetchWeatherData(
                    TEST_CITY
                )
            }
            assertNull(viewModel.weatherData.value)
        }


    @Test
    fun testFetchWeatherData_throwException() = runTest {
            val exception = RuntimeException("Test Exception")
            Mockito.`when`(weatherUseCase.getWeatherData(TEST_CITY)).thenThrow(exception)
            viewModel.fetchWeatherData(
                TEST_CITY
            )
            assertNull(viewModel.weatherData.value)
        }

    private companion object {
        private const val TEST_CITY = "Edison"
    }
}
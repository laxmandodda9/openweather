package com.demo.openweatherapp.data.repoimpl

import com.demo.openweatherapp.BaseTest
import com.demo.openweatherapp.data.model.*
import com.demo.openweatherapp.data.service.WeatherService
import junit.framework.Assert.*
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class WeatherRepositoryImplTest : BaseTest() {

    @Mock
    private lateinit var weatherService: WeatherService
    private lateinit var weatherRepository: WeatherRepositoryImpl


    @Before
    override fun setup() {
        super.setup()
        weatherRepository = WeatherRepositoryImpl(weatherService)
    }

    @Test
    fun test_getWeatherData_success() = runTest {
        val weatherResponse = WeatherResponse(
            weather = listOf(
                Weather(
                    id = 104,
                    main = "Clouds",
                    description = "overcast clouds",
                    icon = "04n"
                )
            ),
            main = Main(
                temp_max = 289.79,
                temp_min = 284.79,
                feels_like = 283.34,
                pressure = 1023,
                humidity = 51,
                sea_level = 1023,
                grnd_level = 1005,
                temp = 284.79
            ),
            visibility = 10000,
            wind = Wind(
                speed = 0.79,
                deg = 273,
                gust = 0.81
            ),
            rain = Rain(h1 = 10.23),
            clouds = Clouds(all = 100),
            dt = 1686607109,
            sys = Sys(
                country = "USA",
                sunrise = 1686535295,
                sunset = 1686601363,
                type = 12,
                id = 213
            ),
            timezone = 7200,
            id = 2716968,
            name = "Ed",
            cod = 200,
            coord = Coord(lat = 123423.23, lon = 12342.23),
            base = "stations"
        )
        Mockito.`when`(weatherService.getWeatherData(any())).thenReturn(
            Response.success(weatherResponse)
        )

        val result = weatherRepository.getWeatherData(any())
        assertNotNull(result)
        assertEquals(289.79, result?.main?.temp_max)
        assertEquals(289.79, result?.main?.temp_max)
        assertEquals("04n", result?.weather?.icon)
        assertEquals("overcast clouds", result?.weather?.description)
        assertEquals(1023, result?.main?.pressure)
    }

    @Test
    fun test_getWeatherData_failure() = runTest {
        Mockito.`when`(weatherService.getWeatherData(any())).thenReturn(
            Response.error(404, "".toResponseBody(null))
        )
        val result = weatherRepository.getWeatherData(any())
        assertNull(result)
    }
}